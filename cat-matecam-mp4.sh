#!/bin/bash

# This Bash script is intended to aid processing video from the Matecam X7 and cameras based on them such as the Grec X
# night vision POV recorder, as well as similar cameras.
#
# The Matecam / Grec X records video to .MP4 files with a maximum length of 5m 1s per file, with the last second of each
# file being duplicated at the start of the next.
#
# This script uses ffmpeg to trim that duplicated 1s from the end of each file and concatenate them all into a single,
# seamless video without re-encoding and losing quality. This script can also be configured for use with files from
# other cameras (e.g. runcams etc.) to account for different file lengths/overlaps however results with those cannot be
# guaranteed. I have only tested this with files from the Matecam and Runcam Helmet Camera.
#
#
# Usage: cat-matecam-mp4.sh [input path] [output file or path] [option]
#
#
# Where [input path] is a directory containing the Matecam / Grec X videos you want to combine, and
# [output file or path] is either the output path & name or the output path for the resulting file (note: the output
# path cannot be on a FAT-formatted partition - the script will abort if such a path is detected)
#
# All files with the ".MP4" extension (case sensitive) as named by the Matecam that are found directly in the input path
# will be used.
#
# If an input path is not specified, the current directory will be used as the input path.
# If a file is specified as the input, then the path containing that file will be used.
#
# If an output filename is not specified, the output video will be named after the input directory.
# If an output path is not specified, the output video will be placed in the current working directory.
#
# A working directory named "cat-matecam-temp" will temporarily be created in the output directory and if trimming the
# the input files the trimmed versions will be placed there before being merged into the output file, so make sure you
# have enough free space at the target location.
#
#
# Options:
#
# There are currently 2 options available, to allow the script to be used with other cameras that may record with
# different video lengths and different or no overlaps:
#
# -l [length]:
#	This will set the maximum allowed length for each chunk/file, allowing any duplicated video to be removed from the
#	end of each file. The default is 00:05:00.000
#
# -n:
#	Setting this option will have the script simply combine the input files without trimming.
#

if (! type -t ffmpeg &>/dev/null) ; then
	echo "ERROR: ffmpeg does not appear to be installed. Please install ffmpeg before trying to run this script."
	exit 1
fi

InputDir="$1"
MaxChunkLength=""
NoTrim=0
shift

if [ "$#" -gt 0 ] && [ "${1:0:1}" != "-" ] ; then
	OutputFile="$1"
	shift
else
	OutputFile=""
fi

if [ "$#" -gt 0 ] ; then
	case ${1,,} in
		-l)
				MaxChunkLength="$(echo $2)"
				if [[ $MaxChunkLength =~ ^[[:blank:]]{0,}$ ]] ; then
					echo "ERROR: Chunk length not specified!"
					exit 1
				fi
				if [[ $MaxChunkLength =~ [^[:digit:]:.] ]] || [[ ! $MaxChunkLength =~ [[:digit:]] ]] ; then
					echo "ERROR: Format for chunk length should be ##:##:##.###"
					exit 1
				fi
				;;
		-n)
				echo "Merge only: Will not trim files."
				NoTrim=1
				;;
		*)
				echo "ERROR: Unknown argument '$1'!"
				exit 1
				;;
	esac
fi

if [ "$MaxChunkLength" == "" ] ; then
	MaxChunkLength="00:05:00.000"
fi

if [[ "$InputDir"  =~ ^[[:blank:]]{0,}$ ]] ; then
	echo "No input path specified. Using current directory."
	InputDir="$PWD"
fi

InputDir="$(realpath "$InputDir")"

if [ -f "$InputDir" ] ; then
	echo "Specified input is a file. Using containing directory."
	InputDir="$(dirname "$InputDir")"
elif [ ! -e "$InputDir" ] ; then
	echo "ERROR: Specified input path does not exist. Aborting."
	exit 1
fi

if [[ "$OutputFile" =~ ^[[:blank:]]{0,}$ ]] ; then
	OutputFile="$PWD/$(basename "$InputDir").mp4"
elif [ -d "$OutputFile" ] || [ "${OutputFile: -1}" == "/" ] ; then
	OutputFile="$OutputFile/$(basename "$InputDir").mp4"
elif [[ ! "${OutputFile: -4}" =~ .[mM][pP]4$ ]] ; then
	OutputFile="$OutputFile.mp4"
fi

OutputFile="$(realpath -m "$OutputFile")"

if [ "$(df -T "$(dirname "$OutputFile")" | grep -e fat)" != "" ] ; then
	echo -e "FAT filesystem detected at output path.\nPlease use a partition formatted with a Linux filesystem to avoid issues."
	exit 1
fi

TempDir="$(dirname "$OutputFile")/cat-matecam-temp"

echo "Input directory: '$InputDir'"
echo "Output file: '$OutputFile'"

# Find the files to trim + concatenate (or just merge)

readarray -t InputFiles <<<"$(find "$InputDir" -maxdepth 1 -xtype f -name '*.MP4' | sort)"

if [[ "${InputFiles[0]}" =~ ^[[:blank:]]{0,}$ ]] ; then
	echo "ERROR: No input files found at specified path!"
	exit 1
fi

echo "${#InputFiles[@]} input files found."

mkdir -p "$TempDir"

# Trim each video if required (output to temp dir) and add each file to the list for the merge stage

for ((X=0 ; X < ${#InputFiles[@]} ; ++X)) ; do
	if [ "$NoTrim" == "1" ] ; then
		echo "file '"${InputFiles[X]}"'" >>"$TempDir/files.txt"
	else
		TempFile="$TempDir/$(basename "${InputFiles[X]:0: -3}")trimmed.MP4"
		ffmpeg -i "${InputFiles[X]}" -c copy -ss 00:00:00.000 -t "$MaxChunkLength" "$TempFile"
		echo "file '$TempFile'" >>"$TempDir/files.txt"
	fi
done

# Concatenate the videos into a single file and then clean up

ffmpeg -f concat -safe 0 -i "$TempDir/files.txt" -c copy "$OutputFile"

rm -rf "$TempDir"

echo "All done."
