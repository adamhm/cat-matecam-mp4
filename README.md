# Matecam X7 / Grec X night vision POV recorder trim & concatenate script

This is a Bash script intended to aid processing video from the Matecam X7 and cameras based on them such as the Grec X night vision POV recorder, as well as similar cameras.

The Matecam / Grec X records video to .MP4 files with a maximum length of 5m 1s per file, with the last second of each file being duplicated at the start of the next.

This script uses ffmpeg to trim that duplicated 1s from the end of each file and concatenate them all into a single, seamless video without re-encoding and losing quality. This script can also be configured for use with files from other cameras (e.g. runcams etc.) to account for different file lengths/overlaps however results with those cannot be guaranteed. I have only tested this with files from the Grec X/Matecam and Runcam Helmet Camera.

## Usage

Using this script is very simple:


`cat-matecam-mp4.sh [input path] [output file or path] [option]`


Where `input path` is a directory containing the Matecam / Grec X videos you want to combine, and `output file or path` is either the output path & name or the output path for the resulting file (note: the output path cannot be on a FAT-formatted partition - the script will abort if such a path is detected)

All files with the ".MP4" extension (case sensitive) as named by the Matecam that are found directly in the input path will be used.

If an input path is not specified, the current directory will be used as the input path.
If a file is specified as the input, then the path containing that file will be used.

If an output filename is not specified, the output video will be named after the input directory.
If an output path is not specified, the output video will be placed in the current working directory.

A working directory named "cat-matecam-temp" will temporarily be created in the output directory and if trimming the the input files the trimmed versions will be placed there before being merged into the output file, so make sure you have enough free space at the target location.

## Options

There are currently 2 options, to allow the script to be used with other cameras that may record with different video lengths and different or no overlaps:

`-l [length]`

This will set the maximum duration allowed for each chunk/file, allowing any duplicated video to be removed from the end of each file. The default is 00:05:00.000

`-n`

Setting this option will have the script simply combine the input files without trimming.
